use futures::StreamExt;
use prost::Message;
use serde::Deserialize;
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Cursor, Read};
use std::sync::Arc;

mod tensorflow;

#[derive(Deserialize)]
struct VocabularyEntry {
    #[serde(rename = "Index")]
    pub index: usize,
    #[serde(rename = "Vertical1")]
    pub vertical1: String,
}

async fn process_record(subset: &[u8], _vertical1: Arc<HashMap<usize, String>>) -> Option<String> {
    let result = tensorflow::Example::decode(&mut Cursor::new(subset)).unwrap();
    let id = String::from_utf8_lossy(
        match result
            .clone()
            .features
            .unwrap()
            .feature
            .get("id")
            .unwrap()
            .kind
            .as_ref()
            .unwrap()
        {
            tensorflow::feature::Kind::BytesList(bytes) => &bytes.value.get(0).unwrap(),
            _ => {
                unimplemented!()
            }
        },
    )
    .to_string();

    let labels = match result
        .features
        .unwrap()
        .feature
        .get("labels")
        .unwrap()
        .kind
        .as_ref()
        .unwrap()
    {
        tensorflow::feature::Kind::Int64List(bytes) => bytes.value.clone(),
        _ => {
            unimplemented!()
        }
    };

    let result = reqwest::get(format!(
        "http://data.yt8m.org/2/j/i/{}/{}.js",
        &id[0..2],
        &id,
    ))
    .await
    .unwrap();

    let mut output_string = String::new();
    if result.status() == 200 {
        let result = result.text().await.unwrap();
        let result = &result[10..result.len() - 3];

        output_string += &format!("{},", result);
    } else {
        return None;
    }

    //output_string += &labels
    //    .iter()
    //    .map(|item| vertical1.get(&(*item as _)).unwrap().to_string())
    //    .collect::<HashSet<String>>()
    //    .into_iter()
    //    .collect::<Vec<String>>()
    //    .join(" ");
    output_string += &labels
        .into_iter()
        .map(|item| item.to_string())
        .collect::<Vec<String>>()
        .join(",");

    Some(output_string)
}

#[tokio::main]
async fn main() {
    if env::args().len() != 3 {
        println!(
            "Invalid number of arguments. Expected format: ./{} <vocabulary.csv> <tfrecord_file>",
            env::args().next().unwrap()
        );
        std::process::exit(1);
    }

    let vocabulary = File::open(env::args().nth(1).unwrap()).unwrap();
    let reader = BufReader::new(vocabulary);
    let mut rdr = csv::Reader::from_reader(reader);

    let mut vertical1 = HashMap::new();

    for line in rdr.deserialize() {
        let vocabulary_entry: VocabularyEntry = line.unwrap();

        vertical1.insert(vocabulary_entry.index, vocabulary_entry.vertical1);
    }
    let vertical1 = Arc::new(vertical1);

    let mut file = File::open(env::args().nth(2).unwrap()).unwrap();
    let mut tmp = Vec::new();
    file.read_to_end(&mut tmp).unwrap();

    let mut buffer = &tmp[..];
    let mut futures = Vec::new();
    while !buffer.is_empty() {
        let subset_size = u64::from_le_bytes([
            buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7],
        ]);

        let subset = &buffer[12..subset_size as usize + 12];

        futures.push(process_record(subset, vertical1.clone()));

        // Offset cursor
        buffer = &buffer[subset_size as usize + 12 + 4..];
    }

    let mut output_file = File::create(format!("{}.txt", env::args().nth(2).unwrap())).unwrap();
    for line in futures::stream::iter(futures)
        .buffer_unordered(512)
        .collect::<Vec<_>>()
        .await
    {
        if let Some(line) = line {
            writeln!(output_file, "{}", line).unwrap();
        }
    }
}
