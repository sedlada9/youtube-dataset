use std::io::Result;

fn main() -> Result<()> {
    prost_build::Config::new()
        .out_dir("./src/")
        .compile_protos(&["./protos/example.proto"], &["./protos"])?;

    Ok(())
}
